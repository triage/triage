Triage
======

Triage es una aplicación web que brinda soporte al procedimiento de Triage ejecutado en el contexto de una guardia hospitalaria. El procedimiento particular implementado por esta aplicación fue definido por el Doctor Luis Reggiani y su equipo.

La aplicación fue desarrollada inicialmente por Marcia Tejeda y Nestor Muñoz en el contexto de su trabajo final de la carrera de Técnico Universitario en Programación Informática en la Universidad Nacional de Quilmes, Argentina. El trabajo fue desarrollado con la dirección y soporte del Ing. Nicolás Paez y del Dr. Pablo Martínez López.

Código fuente disponible en: sources
Módulo Puppet para provision automatizado en: puppet

Para realizar una instalación manual simplificada en un servidor Linux/Ubuntu:

1. Instalar PostgreSQL 9.3
2. Crear un base de datos llamada _triage_prod_
3. Crear un usuario _postgres_ con clave _postgres_ y darle permisos sobre la base recien creada
4. Instalar Java 7
5. Obtener el código de la aplicación y generar el binario. Primero compilar ejecutando _./grailsw compile_ y luego generar el ejecutable standalone ejecutando _./grailsw prod build-standalone triage-jar --jetty_ (esto hace falta hacerlo en el servido y no requiere la instalación de grails, basta con tener el Java jdk7)
6. Poner archivo resultante del paso anterior (triage.jar) en la carpeta _/var/www/triage_ (crear esta estrucvtura de carpeta que posiblemente no exita)
7. Poner en la misma carpeta el archivo de configuración: https://gitlab.com/triage/triage-puppet/raw/master/files/triage-config.groovy
8. Crear el archivo _/etc/init/triage.conf_ con el contenido https://gitlab.com/triage/triage-puppet/raw/master/templates/triage.conf.erb
9. Instalar Nginx
10. Crear el archivo _/etc/nginx/sites-available/triage.com.conf_ con el contenido https://gitlab.com/triage/triage-puppet/raw/master/templates/triage.com.conf.erb
11. Crear el link simbólico _/etc/nginx/sites-enabled/triage.com.conf_ apuntando al archivo creado en el paso anterior.
12. Crear en el sistema operativo el usuario _triage_ y ponerlo como owner de /var/www/triage
13. Asegurarse que PostgreSQL está corriendo y el usuario creado tiene acceso
13. Iniciar el servicio triage: sudo service triage restart
14. Reiniciar el nginx: sudo service nginx restart

Notas:
* La aplicación al iniciar por primera vez se encargará de crear la estructura de tablas necesaria para su funcionamiento, pero la base debe estar creada previamente.
* El nombre de la base de datos y el usuario de conexión pueden ser distintos a los especificados en los pasos 2 y 3, pero ello requiere modificar el archivo de configuración del paso 7



